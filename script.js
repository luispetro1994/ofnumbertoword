var numero ="";
var lang = "";

changeLanguage();
cargarIdiomas();

const darkModeToggle = document.getElementById("darkModeToggle");
const body = document.body;

darkModeToggle.addEventListener("change", () => {
  body.classList.toggle("dark-mode");
  if (body.classList.contains("dark-mode")) {
    localStorage.setItem("darkMode", "enabled");
  } else {
    localStorage.setItem("darkMode", "disabled");
  }
});

// Verificar el estado almacenado en localStorage
if (localStorage.getItem("darkMode") === "enabled") {
  body.classList.add("dark-mode");
  darkModeToggle.checked = true;
} else {
  body.classList.remove("dark-mode");
}

const copiarBoton = document.getElementById("copiar");

copiarBoton.addEventListener("click", () => {
  // Texto que se quiere copiar
  const textoACopiar =document.getElementById('resultadoLabel').textContent;

  // Crear un elemento input temporal
  const input = document.createElement("input");
  input.value = textoACopiar;
  document.body.appendChild(input);

  // Seleccionar el texto del elemento input
  input.select();

  // Copiar el texto al portapapeles
  document.execCommand("copy");

  // Eliminar el elemento input temporal
  document.body.removeChild(input);

  // Mostrar un mensaje al usuario
  mostrarMensaje("Texto copiado en portapapeles!", 1500);
});


async function convertirNumero() {

    changeLanguage();
    var select = document.getElementById('languageSelect');

    const data = { "numero": this.numero,
                    "lang": select.value 
                };

    try {
        const response = await fetch('https://y13p2mea2i.execute-api.us-east-1.amazonaws.com/test/toWords', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const responseData = await response.json();

        if (response.status === 200) {
            document.getElementById('resultadoLabel').textContent = responseData.body;
        } else {
            alert('Error al convertir el número');
        }

    } catch (error) {
        console.error('Error:', error);
        alert('Ocurrió un error al comunicarse con la API');
    }
}


function changeLanguage(){
    
    this.numero = document.getElementById('numeroInput').value;
    var checkbox = document.getElementById('languageSwitch');

    if(checkbox.checked) {
        document.getElementById('buttonText').textContent = "Convertir";
        document.getElementById('titleText').textContent = "Convertir Número a Palabras";
        document.getElementById('numeroInput').placeholder= "Ingrese un número";
        document.getElementById('titleMain').textContent = "Convertir de números a palabras";
    }else{
        document.getElementById('buttonText').textContent = "Convert";
        document.getElementById('titleText').textContent = "CONVERT NUMBER TO WORDS";
        document.getElementById('numeroInput').placeholder= "Enter a number";
        document.getElementById('titleMain').textContent = "Conver of number to word";
    }
}

function mostrarMensaje(mensaje, tiempo) {
    var mensajeElemento = document.getElementById("mensaje");
    mensajeElemento.innerText = mensaje;
    mensajeElemento.style.display = "block";

    setTimeout(function() {
      mensajeElemento.style.display = "none";
    }, tiempo);
  }

  function cargarIdiomas() {
    // Obtener el elemento select
    var select = document.getElementById('languageSelect');
  
    // Obtener el JSON con los idiomas y sus parámetros
    fetch('idioms.json')
      .then(response => response.json())
      .then(data => {
        // Este código se ejecuta cuando los datos JSON se han cargado correctamente
        var idiomas = data; // Asigna los datos a la variable idiomas
  
        // Agregar opciones al select
        for (var idiomaAbreviatura in idiomas) {
          var nombreIdioma = idiomas[idiomaAbreviatura]; // Obtener el nombre del idioma
          var option = document.createElement('option');
          option.value = idiomaAbreviatura; // Asignar el valor de la abreviatura como el valor de la opción
          option.text = nombreIdioma; // Mostrar el nombre del idioma como el texto de la opción
          select.add(option);
        }
  
        // Agregar un evento de cambio al select
        select.addEventListener('change', function() {
          var seleccionado = select.value; // Obtener la abreviatura del idioma seleccionado
          var nombreIdiomaSeleccionado = idiomas[seleccionado]; // Obtener el nombre del idioma seleccionado
          console.log("Idioma seleccionado:", seleccionado, "Nombre:", nombreIdiomaSeleccionado);
          this.lang = seleccionado;
        });
  
      })
      .catch(error => {
        console.error('Error al cargar el archivo JSON:', error);
      });
  }


